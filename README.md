# BipartiteD3 v0.3.1

This R package is a fork/update of [bipartiteD3: Interactive Bipartite Graphs](https://CRAN.R-project.org/package=bipartiteD3) (v0.3.0) by Chris Terry.

## Improvements from [0.3.0](https://CRAN.R-project.org/package=bipartiteD3)
- Remove empty edges (["source", "target", 0]) befor JS generation for memory usage.
- Add TextLabsPos option to set X position of Primary and Secondary labels

## Install

```R
 install.packages("https://gitlab.huma-num.fr/dhune/bipartiteD3/-/raw/main/bipartiteD3_0.3.1.tar.gz", repos = NULL, type = "source")
```

